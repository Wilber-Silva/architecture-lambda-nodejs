'use strict';

const Joi = require('joi');
const CNPJ = require('./joiValidators/cnpj');
const CPF = require('./joiValidators/cpf');
const analyzeKey = require('./joiValidators/analyzeKey');

module.exports = {
  validateKey: Joi.extend({
    name: 'document',
    base: Joi.string(),
    language: {
      keyAccess: 'is an invalid Key Access number'
    },
    rules: [{
      name: 'keyAccess',
      validate(params, value, state, options) {
        const respOfKey = analyzeKey.validStringValueCallOnly(value);
        if (respOfKey.error === true) {
          return this.createError('keyAccess', {
            v: value
          }, state, options);
        }
        return value;
      }
    }]
  }),
  validate: Joi.extend({
    name: 'document',
    base: Joi.string(),
    language: {
      cnpj: 'is an invalid CNPJ number',
      cpf: 'is an invalid CPF number'
    },
    rules: [{
      name: 'cnpj',
      validate(params, value, state, options) {
        if (!CNPJ.isValid(value)) {
          return this.createError('cnpj', {
            v: value
          }, state, options);
        }
        return value;
      }
    },
    {
      name: 'cpf',
      validate(params, value, state, options) {
        if (!CPF.isValid(value)) {
          return this.createError('cpf', {
            v: value
          }, state, options);
        }
        return value;
      }
    }]
  })
};

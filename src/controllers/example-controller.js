'use strict'

const { responseSuccess, responseError } = require('./../utils/response')
const service = require('./../services/example-service')
const validator = require('./../validation/example-validation')

module.exports.find = async (event, context) => {
  context.callbackWaitsForEmptyEventLoop = false
  try {
    const params = event.queryStringParameters
    await validator.find.validate(params, { abortEarly: false, convert: false })
    const response = await service.find(params)
    return responseSuccess(response)
  } catch (error) {
    return responseError(error)
  }
}

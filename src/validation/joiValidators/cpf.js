'use strict';

const BLACKLIST = [
  '00000000000',
  '11111111111',
  '22222222222',
  '33333333333',
  '44444444444',
  '55555555555',
  '66666666666',
  '77777777777',
  '88888888888',
  '99999999999',
  '12345678909'
];

const LOOSE_STRIP_REGEX = /[^\d]/g;

function verifierDigit(numbers) {
  const numbersAux = numbers.split('').map(number => parseInt(number, 10));

  const modulus = numbersAux.length + 1;

  const multiplied = numbersAux.map((number, index) => number * (modulus - index));

  const mod = multiplied.reduce((buffer, number) => buffer + number) % 11;

  return (mod < 2 ? 0 : 11 - mod);
}

const CPF = {};

CPF.strip = function strip(number) {
  return (number || '').toString().replace(LOOSE_STRIP_REGEX, '');
};

CPF.isValid = function isValid(number) {
  const stripped = this.strip(number);

  // CPF must be defined
  if (!stripped) {
    return false;
  }

  // CPF must have 11 chars
  if (stripped.length !== 11) {
    return false;
  }

  // CPF can't be blacklisted
  if (BLACKLIST.indexOf(stripped) >= 0) {
    return false;
  }

  let numbers = stripped.substr(0, 9);
  numbers += verifierDigit(numbers);
  numbers += verifierDigit(numbers);

  return numbers.substr(-2) === stripped.substr(-2);
};

module.exports = CPF;

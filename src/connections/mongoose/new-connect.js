'use strict';

const mongoose = require('mongoose');

const createConnection = () => mongoose.createConnection(
  process.env.MONGODB,
  {
    useNewUrlParser: true,
    poolSize: 1,
    bufferCommands: false, // Disable mongoose buffering
    bufferMaxEntries: 0
    // socketTimeoutMS: 2500,
    // keepAlive: true
  }
);

module.exports = {
  createConnection
};

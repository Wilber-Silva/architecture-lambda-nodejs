'use strict';

module.exports = {
  validDigitAnswer(digit, index) {
    const forceFalse = false;
    const lastValueOfString = parseInt(index.charAt(index.length - 1), 10);
    const lastDigit = digit === lastValueOfString ? forceFalse : true;
    return lastDigit;
  },
  acumulateValue(index) {
    let i = 42;
    const multi = [2, 3, 4, 5, 6, 7, 8, 9];
    let sum = 0;
    while (i >= 0) {
      for (let m = 0; m < multi.length && i >= 0; m += 1) {
        // eslint-disable-next-line security/detect-object-injection
        sum += parseInt(index[i], 10) * parseInt(multi[m], 10);
        i -= 1;
      }
    }
    return sum;
  },
  validStringValueCallOnly(index) {
    let sum = 0;
    const eleven = 11;
    const indexLength = index.length === 44;
    let digit = false;
    const digitBuilt = { error: true, errorLength: true, errorDigit: true };

    if (indexLength === true) {
      digitBuilt.errorLength = false;
      sum = this.acumulateValue(index);
      const leftover = sum % eleven;
      if (leftover === 0 || leftover === 1) {
        digit = 0;
        digitBuilt.errorDigit = this.validDigitAnswer(digit, index);
      }
      else {
        digit = eleven - leftover;
        digitBuilt.errorDigit = this.validDigitAnswer(digit, index);
      }
    }
    if (digitBuilt.errorLength === false && digitBuilt.errorDigit === false) {
      digitBuilt.error = false;
    }
    return digitBuilt;
  }

};

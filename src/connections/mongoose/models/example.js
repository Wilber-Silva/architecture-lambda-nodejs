const mongoose = require('mongoose')
const mongoosePaginate = require('mongoose-paginate')

const STATUS_ANALYZE = 'analyze'
const STATUS_APPROVED = 'approved'

const example = new mongoose.Schema({
  name: { type: String },
  status: { type: String, enum: [STATUS_ANALYZE, STATUS_APPROVED] },
  email: { type: String, required: true },
  cnpj: { type: String, required: true }
})

example.plugin(mongoosePaginate)

module.exports = mongoose.model('example', example)
module.exports = {
    STATUS_APPROVED,
    STATUS_ANALYZE    
}

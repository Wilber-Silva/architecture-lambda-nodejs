module.exports.responseSuccess = (body) => {
  return {
    statusCode: 200,
    body: JSON.stringify(body)
  }
}

module.exports.responseError = (body) => {
  if (body.isJoi) {
    body.statusCode = 422
    body.errorCode = 4221
    body.message = body.details[0].message
  }
  return {
    statusCode: body.statusCode,
    body: JSON.stringify({ errorCode: body.errorCode, message: body.message })
  }
}

module.exports.fieldInvalid = (body) => {
  return {
    statusCode: 422,
    body: JSON.stringify({errorCode: 4221, message: body.message })
  }
}

module.exports.responseInternalError = (body) => {
  return {
    statusCode: 500,
    body: JSON.stringify({errorCode: 500, message: body.message })
  }
}

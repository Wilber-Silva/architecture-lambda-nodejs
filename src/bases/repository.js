/*eslint-disable*/

'use strict';

const co = require('co');
const newConnect = require('./../connections/mongoose/new-connect');

const conn = {
  contents: null,
  inspirations: null,
  invoices: null,
  likes: null,
  quizanswers: null,
  quizzes: null,
  users: null
};

function repository(_model, nameModel) {
  const Model = _model;

  const find = (params, page, limit, select, sort) => co(function* () {
    let Mdl;
    if (conn[nameModel] && conn[nameModel].db.serverConfig.isConnected()) {
      Mdl = conn[nameModel].model(nameModel);
    }
    else {
      conn[nameModel] = yield newConnect.createConnection();
      Mdl = conn[nameModel].model(nameModel, Model);
    }
    return yield Mdl.paginate(params, {
      page,
      limit,
      select,
      sort
    });
	});

  const save = params => co(function* () {
    let Mdl;
    if (conn[nameModel] && conn[nameModel].db.serverConfig.isConnected()) {
      Mdl = conn[nameModel].model(nameModel);
    }
    else {
      conn[nameModel] = yield newConnect.createConnection();
      Mdl = conn[nameModel].model(nameModel, Model);
    }
    const saved = new Mdl(params);
    return yield saved.save();
  });

  const update = (filter, params) => co(function* () {
    let Mdl;
    if (conn[nameModel] && conn[nameModel].db.serverConfig.isConnected()) {
      Mdl = conn[nameModel].model(nameModel);
    }
    else {
      conn[nameModel] = yield newConnect.createConnection();
      Mdl = conn[nameModel].model(nameModel, Model);
    }
    return yield Mdl.update(filter, {
      $set: params
    });
  });

  // Don't use Remove, because Cannot use retryable writes with limit=0
  const remove = params => co(function* () {
    let Mdl;
    conn[nameModel] = yield newConnect.createConnection();
    /* istanbul ignore else */
    if (conn[nameModel].db.serverConfig.isConnected()) {
      Mdl = conn[nameModel].model(nameModel, Model);
    }
    return yield Mdl.deleteOne(params);
  });

  return {
    find,
    save,
    update,
    remove
  };
}

module.exports = repository;

const axios = require('axios');

const http = axios.create();

const find = (endpoint, id, params = {}) => http.get(`${endpoint}/${id}`, { params });

const list = (endpoint, params = {}) => http.get(endpoint, { params });

const create = (endpoint, params) => http.post(endpoint, params);

const update = (endpoint, id, params) => http.put(`${endpoint}/${id}`, params);

const remove = (endpoint, id) => http.delete(`${endpoint}/${id}`)

module.exports = {
    find,
    list,
    create,
    update,
    remove
}
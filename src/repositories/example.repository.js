const baseRepository = require('./../bases/repository')
const model = require('./../connections/mongoose/models/example')

const nameModel = 'invoices'
const repository = new (baseRepository)(model, nameModel)

module.exports = {
  find: repository.find
}

'use strict';

const Joi = require('joi');
Joi.objectId = require('joi-objectid')(Joi);

const customJoi = require('./customJoi');
const {STATUS_ANALYZE, STATUS_APPROVED} = require('./../connections/mongoose/models/example');

module.exports = {
    find: Joi.object().keys({
        name: Joi.string(),
        email: Joi.string().required(),
        cnpj: customJoi.validate.document().cnpj().max(14).required(),
        status: Joi.string().valid(STATUS_ANALYZE, STATUS_APPROVED).trim()
      })
}